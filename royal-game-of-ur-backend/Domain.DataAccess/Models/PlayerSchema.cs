﻿using Domain.Models;

namespace Domain.DataAccess.Models
{
    public static class PlayerSchema
    {
        public static string Table => $"{nameof(Player).ToSnakeCase()}s";

        public static class Columns
        {
            public static string Id => nameof(Player.Id).ToSnakeCase();
            public static string Name => nameof(Player.Name).ToSnakeCase();
            public static string PlayerIndex => nameof(Player.PlayerIndex).ToSnakeCase();
        }
    }
}
