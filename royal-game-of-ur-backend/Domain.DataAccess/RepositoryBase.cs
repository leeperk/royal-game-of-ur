﻿using SqlKata;
using SqlKata.Compilers;
using System.Data;

namespace Domain.DataAccess
{
    public  class RepositoryBase
    {
        protected IDbConnection Connection { get; }

        public RepositoryBase(IDbConnection connection)
        {
            Connection = connection;
            Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;
        }

        protected SqlResult Compile(Query query)
        {
            var compiler = new PostgresCompiler();
            return compiler.Compile(query);
        }
    }
}
