﻿using System.Data;
using Dapper;
using Domain.DataAccess.Interfaces;
using Domain.DataAccess.Models;
using Domain.Models;
using SqlKata;

namespace Domain.DataAccess
{
    public class PlayerRepository : RepositoryBase, IPlayerRepository
    {
        public PlayerRepository(IDbConnection connection) : base(connection) { }

        public async Task<IEnumerable<Player>> GetAll()
        {
            var query = new Query(PlayerSchema.Table)
                .OrderBy(PlayerSchema.Columns.Name);
            var compiledQuery = Compile(query);

            return await Connection.QueryAsync<Player>(compiledQuery.Sql, compiledQuery.NamedBindings);
        }
    }
}
