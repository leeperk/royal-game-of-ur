﻿using Domain.Models;

namespace Domain.DataAccess.Interfaces
{
    public interface IPlayerRepository
    {
        Task<IEnumerable<Player>> GetAll();
    }
}
