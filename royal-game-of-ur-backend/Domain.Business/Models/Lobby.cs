﻿namespace Domain.Business.Models
{
    public class Lobby
    {
        public IList<string> WaitingPlayers { get; set; } = new List<string>();
    }
}
