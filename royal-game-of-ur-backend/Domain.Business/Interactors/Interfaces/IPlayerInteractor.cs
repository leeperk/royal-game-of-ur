﻿using Domain.Models;

namespace Domain.Business.Interactors.Interfaces
{
    public interface IPlayerInteractor
    {
        Task<IEnumerable<Player>> GetAll();
    }
}
