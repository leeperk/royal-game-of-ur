﻿using Domain.Business.Interactors.Interfaces;
using Domain.DataAccess.Interfaces;
using Domain.Models;

namespace Domain.Business.Interactors
{
    public class PlayerInteractor: IPlayerInteractor
    {
        private readonly IPlayerRepository _playerRepository;

        public PlayerInteractor(IPlayerRepository playerRepository)
        {
            _playerRepository = playerRepository;
        }

        public async Task<IEnumerable<Player>> GetAll()
        {
            return await _playerRepository.GetAll();
        }
    }
}
