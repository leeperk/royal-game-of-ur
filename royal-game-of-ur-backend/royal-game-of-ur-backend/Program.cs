using System.Data;
using System.Text.Json;
using System.Text.Json.Serialization;
using Domain.Business.Interactors;
using Domain.Business.Interactors.Interfaces;
using Domain.DataAccess;
using Domain.DataAccess.Interfaces;
using Domain.Identity.Services;
using Domain.Identity.Services.Interfaces;
using Domain.Responses;
using Microsoft.OpenApi.Models;
using Npgsql;
using royal_game_of_ur_backend.SignalR;

namespace royal_game_of_ur_backend
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            builder.Configuration.AddUserSecrets<Program>(true);

            IdentityServices.Configure(builder.Services, builder.Configuration, "DefaultConnection");

            builder.Services.AddControllers()
                .AddJsonOptions(o => o.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()))
                .AddJsonOptions(o => o.JsonSerializerOptions.Converters.Add(new JsonDateTimeIsoConverter()))
                ;
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen((o) =>
            {
                o.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    In = ParameterLocation.Header
                    , Description = "Please enter token"
                    , Name = "Authorization"
                    , Type = SecuritySchemeType.Http
                    , BearerFormat = "JWT"
                    , Scheme = "bearer"
                });
                o.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type=ReferenceType.SecurityScheme
                                , Id="Bearer"
                            }
                        },
                        new string[]{}
                    }
                });
            });
            builder.Services
                .AddSingleton<IDbConnection>((sp) => new NpgsqlConnection(builder.Configuration.GetConnectionString("DefaultConnection")))
                .AddScoped<IPlayerInteractor, PlayerInteractor>()
                .AddScoped<IPlayerRepository, PlayerRepository>()
                .AddScoped<IAuthenticationService, AuthenticationService>();
                ;
            builder.Services.Configure<Microsoft.AspNetCore.Http.Json.JsonOptions>(options =>
            {
                options.SerializerOptions.PropertyNameCaseInsensitive = false;
                options.SerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
            });


            builder.Services.AddSignalR()
                .AddJsonProtocol(o =>
                {
                    o.PayloadSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                });

            var app = builder.Build();

            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseAuthorization();

            app.MapControllers();

            app.MapHub<SignalRHub>("/hub");

            app.Run();
        }
    }
}
