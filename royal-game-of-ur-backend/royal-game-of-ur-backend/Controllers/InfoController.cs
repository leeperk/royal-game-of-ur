﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace royal_game_of_ur_backend.Controllers;

[Route("[controller]/[action]")]
[ApiController]
[Authorize]
public class InfoController : ControllerBase
{
    [HttpGet]
    [ActionName("Get")]
    public IActionResult Get()
    {
        return Ok("I am Authorize");
    }
}