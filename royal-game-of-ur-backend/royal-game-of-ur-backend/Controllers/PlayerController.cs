using Domain.Business.Interactors.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace royal_game_of_ur_backend.Controllers;

[ApiController]
[Authorize]
[Route("[controller]/[action]")]
public class PlayerController : Controller
{
    private readonly ILogger<PlayerController> _logger;
    private readonly IPlayerInteractor _playerInteractor;

    public PlayerController(ILogger<PlayerController> logger, IPlayerInteractor playerInteractor)
    {
        _logger = logger;
        _playerInteractor = playerInteractor;
    }

    [HttpGet]
    [ActionName("GetAll")]
    public async Task<ActionResult> GetAll()
    {
        return Json(await _playerInteractor.GetAll());
    }
}