﻿using Domain.Identity.Models;
using Domain.Identity.Services.Interfaces;
using Domain.Responses.Api;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace royal_game_of_ur_backend.Controllers;

[Route("[controller]/[action]")]
[ApiController]
public class AuthController : ControllerBase
{
    private readonly IAuthenticationService _authenticationService;

    public AuthController(IAuthenticationService service)
    {
        _authenticationService = service;
    }

    [HttpPost]
    [ActionName("Register")]
    public async Task<ActionResult> Register(RegisterUser user)
    {
        var response = await _authenticationService.RegisterNewUser(user);
        return new JsonResult(
            new ApiResponseBuilder<bool>()
                .AddData(response.Data)
                .AddInformation(response.Information)
                .BuildResult()
        );
    }

    [HttpPost]
    [ActionName("Authenticate")]
    public async Task<IActionResult> Authenticate(LoginUser user)
    {
        var response = await _authenticationService.Authenticate(user);
        Response.Cookies.Append("auth", response.Data ?? "", new CookieOptions
        {
            HttpOnly = true,
            SameSite = SameSiteMode.Strict,
            Secure = true
        });

        var externalLogins = await _authenticationService.GetExternalLogins();

        return new JsonResult(
            new ApiResponseBuilder()
                .AddInformation(response.Information)
                .BuildResult()
        );
    }

    [HttpPost]
    [ActionName("SignOut")]
    public new IActionResult SignOut()
    {
        Response.Cookies.Delete("auth");
        return new JsonResult(
            new ApiResponseBuilder<bool>()
                .AddData(true)
                .BuildResult()
        );
    }

    [HttpGet]
    [Authorize]
    [ActionName("Check")]
    public async Task<IActionResult> Check()
    {
        return new JsonResult(
           new ApiResponseBuilder<bool>()
              .AddData(User.Identity?.IsAuthenticated ?? false)
              .BuildResult()
        );
    }
    
}
