﻿using Microsoft.AspNetCore.SignalR;

namespace royal_game_of_ur_backend.SignalR
{
    public class SignalRHub: Hub
    {
        public async Task NewMessage(string user, string message)
        {
            await Clients.All.SendAsync("messageReceived", user, message);
        }
    }
}
