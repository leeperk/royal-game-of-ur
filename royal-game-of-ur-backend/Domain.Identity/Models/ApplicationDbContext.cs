﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Domain.Identity.Models;

public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        builder.HasDefaultSchema("Identity");
        builder
            .Entity<IdentityUserRole<string>>(e => e.ToTable("UserRoles"))
            .Entity<IdentityUserClaim<string>>(e => e.ToTable("UserClaims"))
            .Entity<IdentityUserLogin<string>>(e => e.ToTable("UserLogins"))
            .Entity<IdentityUserToken<string>>(e => e.ToTable("UserTokens"))
            .Entity<IdentityRoleClaim<string>>(e => e.ToTable("RoleClaims"))
            .Entity<IdentityRole>(e => e.ToTable("Roles"))
            .Entity<ApplicationUser>(e => e.ToTable("Users"))
            ;
        builder
            .Entity<ApplicationUser>(e => e.Property(u => u.DisplayName).IsRequired().HasMaxLength(50))
            ;
    }
}