﻿using Microsoft.AspNetCore.Identity;

namespace Domain.Identity.Models;

public class ApplicationUser: IdentityUser
{
    public string DisplayName { get; set; } = "";
}
