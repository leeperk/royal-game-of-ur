﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Identity.Models;

public class RegisterUser
{
    [EmailAddress]
    public required string Email { get; set; }

    public required string DisplayName { get; set; }

    public required string Password { get; set; }
}