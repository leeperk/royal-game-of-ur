﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Domain.Identity.Migrations
{
    /// <inheritdoc />
    public partial class addDisplayNameToUsers : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DisplayName",
                schema: "Identity",
                table: "Users",
                type: "character varying(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DisplayName",
                schema: "Identity",
                table: "Users");
        }
    }
}
