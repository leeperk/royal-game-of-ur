﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Domain.Responses.ServiceLayer;
using Domain.Identity.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using IAuthenticationService = Domain.Identity.Services.Interfaces.IAuthenticationService;

namespace Domain.Identity.Services;

public class AuthenticationService: IAuthenticationService
{
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly RoleManager<IdentityRole> _roleManager;
    private readonly SignInManager<ApplicationUser> _signInManager;
    private readonly IConfiguration _configuration;

    public AuthenticationService(
        UserManager<ApplicationUser> userManager
        , RoleManager<IdentityRole> roleManager
        , SignInManager<ApplicationUser> signInManager
        , IConfiguration configuration
    )
    {
        _userManager = userManager;
        _roleManager = roleManager;
        _signInManager = signInManager;
        _configuration = configuration;
    }

    private JwtSecurityToken GetToken(List<Claim> authClaims)
    {
        var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

        var token = new JwtSecurityToken(
            issuer: _configuration["JWT:ValidIssuer"],
            audience: _configuration["JWT:ValidAudience"],
            expires: DateTime.Now.AddHours(3),
            claims: authClaims,
            signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
        );

        return token;
    }

    public async Task<ServiceLayerResponse<bool>> RegisterNewUser(RegisterUser registerUser)
    {
        var response = new ServiceLayerResponse<bool>();
        try
        {
            var userExists = await _userManager.FindByEmailAsync(registerUser.Email);
            if (userExists != null)
            {
                return response.SetData(false).AddInfo<Error>("User already exists!");
            }

            ApplicationUser user = new()
            {
                UserName = registerUser.Email
                , Email = registerUser.Email
                , DisplayName = registerUser.DisplayName
                , SecurityStamp = Guid.NewGuid().ToString()

            };
            var result = await _userManager.CreateAsync(user, registerUser.Password);

            return result.Succeeded
                ? response.SetData(true)
                : response.SetData(false).AddInfo<Error>("User creation failed! Please check user details and try again.");
        }
        catch (Exception e)
        {
            return response.SetData(false).AddInfo<Error>("Something went wrong registering the new user!");
        }
    }

    public async Task<ServiceLayerResponse<string>> Authenticate(LoginUser loginUser)
    {
        var response = new ServiceLayerResponse<string>();

        try
        {
            var user = await _userManager.FindByEmailAsync(loginUser.Email);
            if (user != null && await _userManager.CheckPasswordAsync(user, loginUser.Password))
            {
                var userRoles = await _userManager.GetRolesAsync(user);

                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.DisplayName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };

                foreach (var userRole in userRoles)
                {
                    authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                }

                var token = GetToken(authClaims);

                return response.SetData(new JwtSecurityTokenHandler().WriteToken(token));
            }
            return response.AddInfo<Unauthorized>("Email and/or password could not be authenticated.  Check both and try again.");
        }
        catch (Exception e)
        {
            return response.AddInfo<Unauthorized>("Something went wrong trying to sign in.  Please try again.");
        }
    }

    public async Task<ServiceLayerResponse<IEnumerable<AuthenticationScheme>>> GetExternalLogins()
    {
        var response = new ServiceLayerResponse<IEnumerable<AuthenticationScheme>>();
        try
        {
            var externalLogins = await _signInManager.GetExternalAuthenticationSchemesAsync();
            return response.SetData(externalLogins);
        }
        catch (Exception e)
        {
            return response.AddInfo<Error>("Something went wrong trying to get external logins.  Please try again.");
        }
    }
}