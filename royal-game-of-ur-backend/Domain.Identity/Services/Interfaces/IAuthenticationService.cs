﻿using Domain.Responses.ServiceLayer;
using Domain.Identity.Models;
using Microsoft.AspNetCore.Authentication;

namespace Domain.Identity.Services.Interfaces;

public interface IAuthenticationService
{
    Task<ServiceLayerResponse<bool>> RegisterNewUser(RegisterUser model);
    Task<ServiceLayerResponse<string>> Authenticate(LoginUser user);
    Task<ServiceLayerResponse<IEnumerable<AuthenticationScheme>>> GetExternalLogins();
}
