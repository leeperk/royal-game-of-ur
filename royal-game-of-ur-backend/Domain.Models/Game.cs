﻿namespace Domain.Models
{
    public class Game
    {
        public Tile[] Board { get; }

        private IDictionary<Players, int[]> _tilePaths { get; } = new Dictionary<Players, int[]>()
        {
            { Players.Player1, new [] { 0, 1, 2, 3, 8, 9, 10, 11, 12, 13, 14, 15, 17, 16 } }
            , { Players.Player1, new [] { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 19, 18 } }
    };

        public Game()
        {
            Board = new Tile[20];
            InitializeBoard();
        }

        // ┌───┬───┬───┬───┐       ┌───┬───┐
        // │ 0 │ 1 │ 2 │ 3 │       │16 │17 │
        // ├───┼───┼───┼───┼───┬───┼───┼───┤
        // │ 8 │ 9 │10 │11 │12 │13 │14 │15 │
        // ├───┼───┼───┼───┼───┴───┼───┼───┤
        // │ 4 │ 5 │ 6 │ 7 │       │18 │19 │
        // └───┴───┴───┴───┘       └───┴───┘

        private void InitializeBoard()
        {
            int index;
            for (index = 0; index < 20; index++)
            {
                Board[index] = new Tile
                {
                    Index = index
                    , NextTile =
                    {
                        [Players.Player1] = _tilePaths[Players.Player1][index]
                        , [Players.Player2] = _tilePaths[Players.Player2][index]
                    }
                };

                switch (index)
                {
                    case 0 or 4:
                        Board[index].IsExtraTurn = true;
                        break;
                    case 11:
                        Board[index].IsExtraTurn = true;
                        Board[index].IsSafe = true;
                        break;
                    case 14 or 15:
                        Board[index].DistanceToToEnd = 4 - (index - 14);
                        break;
                    case 17 or 19:
                        Board[index].DistanceToToEnd = 2;
                        break;
                    case 16 or 18:
                        Board[index].IsExtraTurn = true;
                        Board[index].DistanceToToEnd = 2;
                        break;
                }
            }
        }
    }
}
