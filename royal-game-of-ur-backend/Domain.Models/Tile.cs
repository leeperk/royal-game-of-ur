﻿namespace Domain.Models
{
    public class Tile
    {
        public int Index { get; set; }
        public bool IsSafe { get; set; }
        public bool IsExtraTurn { get; set; }
        public int DistanceToToEnd { get; set; }
        public IDictionary<Players, int> NextTile { get; }

        public Tile()
        {
            NextTile = new Dictionary<Players, int>();
        }
    }
}
