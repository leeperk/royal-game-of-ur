﻿namespace Domain.Models
{
    public class Player
    {
        public int Id { get; set; } = 0;
        public string Name { get; set; } = "";
        public int PlayerIndex { get; set; } = 0;
    }
}
