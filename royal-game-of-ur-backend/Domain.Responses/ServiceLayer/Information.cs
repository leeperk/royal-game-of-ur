﻿namespace Domain.Responses.ServiceLayer;

public abstract class Information
{
    public string Description { get; set; } = "";
}

public class Error : Information { }
public class ValidationError : Error
{
    public string PropertyName { get; set; } = "";
}
public class Warning : Error { }
public class Unauthorized : Error { }
public class Failure : Error { }
public class NotFound : Error { }
public class Conflict : Error { }
public class Success : Information { }
