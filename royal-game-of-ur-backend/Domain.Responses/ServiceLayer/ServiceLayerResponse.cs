﻿namespace Domain.Responses.ServiceLayer;

public class ServiceLayerResponse
{
    public List<Information> Information { get; } = new List<Information>();

    public IEnumerable<Error> GetErrors => Information.Where(i => i is Error).Cast<Error>();

    public bool IsSuccess => Information.Any(i => i is Error) == false;
    public bool HasErrors => Information.Any(i => i is Error);
    public bool IsAuthorizationError => Information.Any(e => e is Unauthorized);
    public bool IsFailure => Information.Any(e => e is Failure);
    public bool IsWarning => Information.Any(e => e is Warning);
    public bool IsNotFound => Information.Any(e => e is NotFound);
    public bool IsValidationError => Information.Any(e => e is ValidationError);
}

public class ServiceLayerResponse<TData> : ServiceLayerResponse
{
    public TData? Data { get; set; }

    public ServiceLayerResponse() { }

    public ServiceLayerResponse(TData data)
    {
        Data = data;
    }

    public ServiceLayerResponse<TData> AddInfo<TInfo>(string message) where TInfo : Information, new()
    {
        Information.Add(new TInfo() { Description = message });
        return this;
    }

    public ServiceLayerResponse<TData> SetData(TData data)
    {
        Data = data;
        return this;
    }
}