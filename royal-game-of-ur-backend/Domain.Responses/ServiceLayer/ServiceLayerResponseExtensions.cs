﻿namespace Domain.Responses.ServiceLayer
{
    public static class ServiceLayerResponseExtensions
    {
        public static ServiceLayerResponse<TData> AddInfo<TInformation, TData>(
            this ServiceLayerResponse<TData> response
            , string message
        ) where TInformation : Information, new()
        {
            response.Information.Add(new TInformation() { Description = message });
            return response;
        }

        public static ServiceLayerResponse<TData> AddInfo<TData>(
            this ServiceLayerResponse<TData> response
            , IEnumerable<Information> information
        )
        {
            response.Information.AddRange(information);
            return response;
        }

        public static ServiceLayerResponse AddInfo<TInformation>(
            this ServiceLayerResponse response
            , string message
        ) where TInformation : Information, new()
        {
            response.Information.Add(new TInformation() { Description = message });
            return response;
        }

        public static ServiceLayerResponse AddInfo(
            this ServiceLayerResponse response
            , IEnumerable<Information> information
        )
        {
            response.Information.AddRange(information);
            return response;
        }

        public static ServiceLayerResponse HandleException(this ServiceLayerResponse response)
        {
            return response.AddInfo<Failure>("An unrecoverable error was encountered.");
        }
    }
}
