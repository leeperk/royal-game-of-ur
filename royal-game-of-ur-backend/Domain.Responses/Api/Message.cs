﻿namespace Domain.Responses.Api;

public class Message
{
    public MessageType Type { get; set; }
    public string Text { get; set; }
    public string Property { get; set; }

    public Message(MessageType type, string text, string propertyName = "")
    {
        Type = type;
        Text = text;
        Property = propertyName;
    }
}
