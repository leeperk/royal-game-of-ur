﻿using Domain.Responses.ServiceLayer;
using Microsoft.AspNetCore.Mvc;

namespace Domain.Responses.Api;

public class ApiResponseBuilder
{
    protected List<Information> Information = new();

    public ActionResult BuildResult()
    {
        return new JsonResult(BuildResultModel());
    }

    public static ApiResponseBuilder<TData> FromServiceLayerResponse<TData>(ServiceLayerResponse<TData> serviceLayer) 
        => new ApiResponseBuilder<TData>().AddData(serviceLayer.Data).AddInformation(serviceLayer.Information);

    public static ApiResponseBuilder FromServiceLayerResponse(ServiceLayerResponse serviceLayer) 
        => new ApiResponseBuilder().AddInformation(serviceLayer.Information);
        
    public virtual ApiResponseBuilder AddInformation(IEnumerable<Information> information)
    {
        Information.AddRange(information);
        return this;
    }

    protected virtual ApiResponse BuildResultModel() =>
        new()
        {
            Messages = Information.ConvertToMessages().ToList()
        };
}
public class ApiResponseBuilder<TData>: ApiResponseBuilder
{
    private TData _data = default!;
        
    public ApiResponseBuilder<TData> AddData(TData data)
    {
        _data = data;
        return this;
    }
    public override ApiResponseBuilder<TData> AddInformation(IEnumerable<Information> information)
    {
        base.AddInformation(information);
        return this;
    }

    protected override ApiResponse BuildResultModel() => GenerateResponseWithData();

    private ApiResponse<TData> GenerateResponseWithData() =>
        new()
        {
            Data = _data
            , Messages = Information.ConvertToMessages().ToList()
        };
}