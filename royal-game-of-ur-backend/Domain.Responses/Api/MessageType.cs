﻿namespace Domain.Responses.Api;

public enum MessageType
{
    Success
    , Warning
    , Error
    , Unauthorized
}
