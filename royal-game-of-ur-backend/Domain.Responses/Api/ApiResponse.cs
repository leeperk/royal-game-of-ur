﻿namespace Domain.Responses.Api;

public class ApiResponse
{
    public IList<Message> Messages { get; set; } = new List<Message>();
}

public class ApiResponse<T> : ApiResponse
{
    public required T Data { get; set; }
}
