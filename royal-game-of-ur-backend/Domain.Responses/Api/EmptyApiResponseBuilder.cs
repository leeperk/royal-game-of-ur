﻿using Domain.Responses.ServiceLayer;

namespace Domain.Responses.Api;

internal class EmptyApiResponseBuilder: ApiResponseBuilder
{
    public override EmptyApiResponseBuilder AddInformation(IEnumerable<Information> information)
    {
        base.AddInformation(information);
        return this;
    }

    public EmptyApiResponseBuilder AddInformation(params Information[] information)
    {
        Information.AddRange(information);
        return this;
    }

    protected override ApiResponse BuildResultModel() =>
        new()
        {
            Messages = Information.ConvertToMessages().ToList()
        };
}