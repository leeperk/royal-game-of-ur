﻿using Domain.Responses.ServiceLayer;

namespace Domain.Responses.Api;

public static class InformationExtensions
{
    public static IEnumerable<Message> ConvertToMessages(this IEnumerable<Information> information)
    {
        return information.Select(ConvertInformationToMessage);
    }
    public static Message ConvertInformationToMessage(this Information information)
    {
        switch (information)
        {
            case ValidationError validationError: return new Message(MessageType.Error, validationError.Description, validationError.PropertyName);
            case Warning: return new Message(MessageType.Warning, information.Description);
            case Unauthorized: return new Message(MessageType.Unauthorized, information.Description);
            case Failure: return new Message(MessageType.Error, information.Description);
            case NotFound: return new Message(MessageType.Error, information.Description);
            case Conflict: return new Message(MessageType.Error, information.Description);
            case Success: return new Message(MessageType.Success, information.Description);
            default: return new Message(MessageType.Error, information.Description);
        }
    }
}
