export interface TileProps {
  isExtraTurn: boolean;
  isSafe: boolean;
  image: string;
}
