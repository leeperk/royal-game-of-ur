import { PropsWithChildren } from "react";
import styled from "styled-components";

export interface ICardProps {
    className?: string;
    backgroundColor?: string;
}

export function Card({ className, backgroundColor = "lightgray", children }: PropsWithChildren<ICardProps>): JSX.Element {
    return <CardFrame className={className} backgroundColor={backgroundColor}>
        {children}
    </CardFrame>
}

Card.Row = function({ children }: PropsWithChildren<{}>) {
    return <CardRow>
        {children}
    </CardRow>
}

interface ICardFrameProps {
    backgroundColor: string;
}
const CardFrame = styled.div
    .withConfig({shouldForwardProp: (propName) => !["backgroundColor"].includes(propName) })
    .attrs(() => ({ className: "card styled-CardFrame" }))<ICardFrameProps>
    `
        background-color: ${(props) => props.backgroundColor};
        margin: 20px;
        padding: 8px;
        overflow: hidden;
    `;
CardFrame.displayName = "CardFrame";

const CardRow = styled.div.attrs(() => ({ className: "styled-CardRow" }))`
    display: flex;
    flex-direction: row;
    padding-top:15px;
`;
CardRow.displayName = "CardRow";
