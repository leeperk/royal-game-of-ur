import { PropsWithChildren } from "react";
import styled from "styled-components";

export interface ICardImageBodyProps {
    className?: string;
    backgroundImage: string;
}

export function CardImageBody({ className = "", backgroundImage = "", children }: PropsWithChildren<ICardImageBodyProps>): JSX.Element {
    return <CardBodyFrame className={className}>
        <CardBodyBackgroundImage src={backgroundImage} />
        <CardBodyContainer>
            {children}
        </CardBodyContainer>
    </CardBodyFrame>
}

const CardBodyFrame = styled.div.attrs(() => ({ className: "card-body styled-CardBodyFrame" }))`
    position: relative;
    padding: 6px;
    width: 100%;
`;
CardBodyFrame.displayName = "CardBodyFrame";

const CardBodyBackgroundImage = styled.img.attrs(() => ({ className: "styled-CardBodyBackgroundImage" }))`
    position: absolute;
    top: 0;
    left: 0;
    max-width: 100%;
    opacity: 0.3;
`;
CardBodyBackgroundImage.displayName = "CardBodyBackgroundImage";

const CardBodyContainer = styled.div.attrs(() => ({ className: "styled-CardBodyContainer" }))`
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
`;
CardBodyContainer.displayName = "CardBodyContainer";
