import { PropsWithChildren } from "react";
import styled from "styled-components";

export function CardHeader({ children }: PropsWithChildren<{}>): JSX.Element {
    return <CardHeaderFrame>
        {children}
    </CardHeaderFrame>
}

export const CardHeaderFrame = styled.div.attrs(() => ({ className: "styled-CardHeaderFrame" }))`
`;
CardHeaderFrame.displayName = "CardHeaderFrame";
