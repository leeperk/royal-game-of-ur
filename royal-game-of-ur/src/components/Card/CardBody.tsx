import { PropsWithChildren } from "react";
import styled from "styled-components";

export interface ICardBodyProps {
    className?: string;
    backgroundColor?: string;
}

export function CardBody({ className = "styled-CardBody", backgroundColor = "white", children }: PropsWithChildren<ICardBodyProps>): JSX.Element {
    return <CardBodyFrame className={className} backgroundColor={backgroundColor}>
        {children}
    </CardBodyFrame>
}

interface ICardBodyFrameProps {
    backgroundColor: string;
}
const CardBodyFrame = styled.div
    .withConfig({shouldForwardProp: (propName) => !["backgroundColor"].includes(propName) })
    .attrs((props) => ({ className: "card-body styled-CardBodyFrame" }))<ICardBodyFrameProps>
    `
        position: relative;
        background-color: ${(props) => props.backgroundColor};
        padding: 6px;
        width: 100%;
    `;
CardBodyFrame.displayName = "CardBodyFrame";
