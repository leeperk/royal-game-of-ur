import styled from "styled-components";

export interface ILabledInputProps {
    labelText: string;
    value: string;
    type?: string;
    autoFocus?: boolean;
    onChange: (text: string) => void;
}

export function LabeledInput(props: ILabledInputProps): JSX.Element {
    return <InputFrame>
        <Input type={props.type} autoFocus={props.autoFocus} value={props.value} onChange={onInputChange} />
        <span className="floating-label">{props.labelText}</span>
    </InputFrame>

    function onInputChange(event: React.ChangeEvent<HTMLInputElement>): void {
        props.onChange(event.target.value);
    }
}

const InputFrame = styled.div.attrs(() => ({ className: "styled-InputFrame" }))`
    position: relative;
    flex-grow: 1;
`;
InputFrame.displayName = "InputFrame";

const Input = styled.input.attrs(() => ({ className: "form-control styled-Input", required: true }))`
    font-size: 14px;

    &:focus ~ .floating-label,
    &:not(:focus):valid ~ .floating-label {
        top: -13px;
        left: 14px;
        font-size: 14px;
        padding-left: 5px;
        padding-right: 5px;
        background-color: white;
    }

    &:not(:focus) ~ .floating-label {
        top: 8px;
        left: 11px;
        font-size: 14px;
    }

    & ~ .floating-label {
        position: absolute;
        pointer-events: none;
        color: gray;
        transition: 0.2s ease all;
    }
`;
Input.displayName = "Input";
