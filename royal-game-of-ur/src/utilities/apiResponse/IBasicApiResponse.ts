import { IMessage } from ".";

export interface IBasicApiResponse {
  messages: IMessage[];
}
