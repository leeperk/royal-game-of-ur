import { MessageType } from ".";

export interface IMessage {
  type: MessageType;
  text: string;
  property?: string;
}
