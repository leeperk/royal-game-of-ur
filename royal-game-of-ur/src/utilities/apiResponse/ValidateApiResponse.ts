import { IBasicApiResponse } from ".";

const successMessageTypes = ["success", "warning"];

export function ValidateApiResponse(response: Response, result: IBasicApiResponse): boolean {
  const errorMessages = result?.messages?.filter((m) => false === successMessageTypes.includes(m.type.toLowerCase())) ?? "";

  return response.status === 200 && errorMessages.length === 0;
}
