import { IBasicApiResponse } from ".";

export interface IApiResponse<T> extends IBasicApiResponse {
  data: T;
}
