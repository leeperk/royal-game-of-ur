import { createApi, fetchBaseQuery, skipToken } from "@reduxjs/toolkit/query/react";

export const apiTags = {
    authCheck: 'authCheck'
} as const;

export const specialIds = {
    LIST: 'LIST'
} as const;

const tagList = Object.values(apiTags);
type TagTypes = typeof tagList[number];

const idList = Object.values(specialIds);
type IdTypes = (typeof idList[number]) | string | number;


export const baseApiSlice = createApi({
    tagTypes: tagList
    , reducerPath: 'api'
    , baseQuery: fetchBaseQuery({ baseUrl: 'api' })
    , endpoints: () => ({ })
});

export function providesList<R extends { id: IdTypes }[] | { id: IdTypes }>(
    resultsWithIds: R | undefined,
    tagType: TagTypes
  ) {
    if (!resultsWithIds) {
      return [{ type: tagType, id: specialIds.LIST }];
    }
    if ("map" in resultsWithIds) {
      return resultsWithIds
      ? [
          { type: tagType, id: specialIds.LIST }
          ,...resultsWithIds.map(({ id }) => ({ type: tagType, id }))
        ]
      : [{ type: tagType, id: specialIds.LIST }];
    }
      return resultsWithIds
      ? [
          { type: tagType, id: specialIds.LIST }
          ,{ type: tagType, id: resultsWithIds.id }
        ]
      : [{ type: tagType, id: specialIds.LIST }];
  }

export function providesItem<R extends { id: IdTypes }>(
  resultsWithIds: R | undefined,
  tagType: TagTypes
) {
  return resultsWithIds
    ? [{ type: tagType, id: resultsWithIds.id }]
    : [];
}

export interface IQueryableProps<TQueryArgs, TResponse> {
  query: (arg: TQueryArgs | typeof skipToken, options?: { skip?: boolean}) => { data?: TResponse; isLoading: boolean; isUninitialized: boolean; };
  queryArgs: TQueryArgs;
}