import { Route, BrowserRouter as Router, Routes } from 'react-router-dom';
import { Welcome } from './domain/Welcome';
import { NewGame } from './domain/NewGame';
import styled from 'styled-components';

const boardImage = require("./images/board.png");

export default function App() {
    return (
        <GameBackground>
        <Router basename={process.env.PUBLIC_URL}>
            <Routes>
                <Route path="/" element={<Welcome />} />
                <Route path="/game" element={<NewGame />} />
            </Routes>
        </Router>
        </GameBackground>
    );
}

const GameBackground = styled.div`
  background-image: linear-gradient(rgb(255,255,255,0.85), rgb(255,255,255,0.85)), url(${boardImage});
  background-size: 50%;
  background-repeat: repeat;
  height: 100vh;
  width: 100vw;
`;
