import { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import styled from "styled-components";
import { Title } from ".";
import { Register, SignIn, useIsSignedInQuery } from "../Auth";

export function Welcome(): JSX.Element {
    const [mode, setMode] = useState<"SignIn"|"Register">("SignIn");
    const { data: isLoggedIn = false } = useIsSignedInQuery();
    const navigate = useNavigate();

    useEffect(() => {
        if (isLoggedIn) {
            navigate( "/game");
        }
    }, [isLoggedIn, navigate]);

    return <WelcomeContainer>
        <Title />
        {mode === "SignIn" && <SignIn onRegister={() => setMode("Register")} />}
        {mode === "Register"  && <Register onSignIn={() => setMode("SignIn")} />}
    </WelcomeContainer>
}

const WelcomeContainer = styled.div`
    height: 100%;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-evenly;
`;
WelcomeContainer.displayName = "WelcomeContainer";
