import styled from "styled-components";

export function Title(): JSX.Element {
    return <>
        <CenteredBody>
            <GameTitle>
                <div>Royal Game of Ur</div>
                <div>Royal Game of Ur</div>
            </GameTitle>
            <PlayText>
                <div>Play the Royal Game of Ur with<br />your friends (or enemies)!</div>
            </PlayText>
        </CenteredBody>
    </>;
}

const CenteredBody = styled.div`
`;

CenteredBody.displayName = "CenteredBody";

const GameTitle = styled.div`
    font-size: 85px;
    font-weight: 700;
    white-space: nowrap;
    & > div:nth-child(1) {
        position: absolute;
        margin-top: 4px;
        margin-left: 2px;
        color: black;
    }
    & > div:nth-child(2) {
        position: relative;
        margin-top: 0;
        color: #a5612a;
    }
`;
GameTitle.displayName = "GameTitle";

const PlayText = styled.div`
    font-size: 40px;
    font-weight: 600;
    text-align: center;
    color: #a5612a;
`;
PlayText.displayName = "PlayText";