import { useNavigate } from "react-router";
import styled from "styled-components";
import { useSignOutMutation } from "../Auth";

export function NewGame() {
    const navigate = useNavigate();
    const [signOut] = useSignOutMutation();

    return <NewGameContainer>
        <button type="button">New Game</button>
        <button type="button">Join Game</button>
        <button type="button" onClick={onSignOut}>Sign Out</button>
    </NewGameContainer>;

    async function onSignOut(): Promise<void> {
        await signOut();
        navigate("/");
    }
}

const NewGameContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`;