import styled from "styled-components";
import { Card } from "../../components/Card";
import { LabeledInput } from "../../components/Form";
import { useState } from "react";

export interface IRegisterProps {
    onSignIn: () => void;
}

export function Register(props: IRegisterProps): JSX.Element {
    const [email, setEmail] = useState("");
    const [displayName, setDisplayName] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");

    return <RegisterCard>
        <h2>Register</h2>
        <CardBody>
            <Card.Row><LabeledInput labelText="Email" autoFocus={true} value={email} onChange={setEmail} /></Card.Row>
            <Card.Row><LabeledInput labelText="Display Name" value={displayName} onChange={setDisplayName} /></Card.Row>
            <Card.Row><LabeledInput type="password" labelText="Password" value={password} onChange={setPassword} /></Card.Row>
            <Card.Row><LabeledInput type="password" labelText="Confirm Password" value={confirmPassword} onChange={setConfirmPassword} /></Card.Row>
            <ActionRow>
                <button type="button" className="btn btn-primary">Register</button>
                <SignInSection><span>Already have an account?</span><SignInLink onClick={onSignIn}>Sign In</SignInLink></SignInSection>
            </ActionRow>
        </CardBody>
    </RegisterCard>;

    function onSignIn(event: React.MouseEvent<HTMLButtonElement, MouseEvent>): void {
        event.preventDefault();
        props.onSignIn();
    }
}

const RegisterCard = styled(Card)`
    width: 400px;

    & button {
        margin-top: 10px;
    }
`;

const CardBody = styled.div.attrs(() => ({ className: "card-body" }))`
    background-color: white;
    padding-top: 6px;
`;

const ActionRow = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;

const SignInSection = styled.div`
    display: flex;
    flex-direction: row;
    align-items: baseline;
    justify-content: center;
`;

const SignInLink = styled.button.attrs({ className: "btn btn-link", type: "button" })`
    margin-top: 0;
    padding-left: 4px;
    padding-right: 0;
    text-decoration: none;
`;
