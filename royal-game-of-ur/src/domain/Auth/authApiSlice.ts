import { apiTags, baseApiSlice } from "../../store/baseApi";
import { IApiResponse, ValidateApiResponse } from "../../utilities/apiResponse";
import { IRegisterUser, ISignInUser } from "./models";

const authApiSlice = baseApiSlice.injectEndpoints({
    endpoints: (builder) => ({
        isSignedIn: builder.query<boolean, void>({
            query: () => ({
                url: "Auth/check"
                , method: "GET"
                , validateStatus: ValidateApiResponse
                , transformResponse: (response: IApiResponse<boolean>) => response.data
            })
            , providesTags: [apiTags.authCheck]
        })
        , authenticate: builder.mutation<IApiResponse<string>, ISignInUser>({
            query: (signInUser) => ({
                url: "Auth/authenticate"
                , method: "POST"
                , body: signInUser
                , validateStatus: ValidateApiResponse
            })
            , invalidatesTags: [apiTags.authCheck]
        })
        , signOut: builder.mutation<IApiResponse<string>, void>({
            query: () => ({
                url: "Auth/signOut"
                , method: "POST"
                , validateStatus: ValidateApiResponse
            })
            , invalidatesTags: [apiTags.authCheck]
        })
        , register: builder.mutation<IApiResponse<boolean>, IRegisterUser>({
            query: (registerUser) => ({
                url: "Auth/register"
                , method: "POST"
                , body: registerUser
                , validateStatus: ValidateApiResponse
            })
            , invalidatesTags: [apiTags.authCheck]
        })

    })
});

export const { useIsSignedInQuery, useAuthenticateMutation, useSignOutMutation, useRegisterMutation } = authApiSlice;