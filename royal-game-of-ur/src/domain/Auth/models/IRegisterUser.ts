export interface IRegisterUser {
    email: string;
    displayName: string;
    password: string;
}
