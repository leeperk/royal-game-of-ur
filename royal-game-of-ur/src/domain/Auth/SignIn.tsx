import styled from "styled-components";
import { Card, CardHeader, CardBody } from "../../components/Card";
import { LabeledInput } from "../../components/Form";
import { ISignInUser } from "./models";
import { useState } from "react";
import { useAuthenticateMutation } from ".";

export interface ISignInProps {
    onRegister: () => void;
}

export function SignIn(props: ISignInProps): JSX.Element {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [authenticate] = useAuthenticateMutation();

    return <SignInCard>
        <CardHeader>
            <h2>Sign In</h2>
        </CardHeader>
        <CardBody>
            <Card.Row><LabeledInput labelText="Email" autoFocus={true} value={email} onChange={setEmail} /></Card.Row>
            <Card.Row><LabeledInput labelText="Password" value={password} type="password" onChange={setPassword} /></Card.Row>
            <ActionRow>
                <button type="button" className="btn btn-primary" onClick={onSignInClick}>Sign In</button>
                <Register><span>Not a member?</span><RegisterLink onClick={onRegister}>Register</RegisterLink></Register>
            </ActionRow>
        </CardBody>
    </SignInCard>;

    async function onSignInClick(): Promise<void> {
        const signInUser: ISignInUser = { email, password };
        await authenticate(signInUser);
    }

    function onRegister(event: React.MouseEvent<HTMLButtonElement, MouseEvent>): void {
        event.preventDefault();
        props.onRegister();
    }
}

export const SignInCard = styled(Card)`
    width: 400px;
    min-width: 290px;
    box-shadow: 8px 8px 10px 0px rgba(0,0,0,0.50);

    & button {
        margin-top: 10px;
    }
`;
SignInCard.displayName = "SignInCard";

const ActionRow = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;
ActionRow.displayName = "ActionRow";

const Register = styled.div`
    display: flex;
    flex-direction: row;
    align-items: baseline;
    justify-content: center;
`;
Register.displayName = "Register";

const RegisterLink = styled.button.attrs({ className: "btn btn-link", type: "button" })`
    margin-top: 0;
    padding-left: 4px;
    padding-right: 0;
    text-decoration: none;
`;
RegisterLink.displayName = "RegisterLink";
